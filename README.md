# Ansible Memcached Role

This is an Ansible role to install [Memcached](https://www.memcached.org/).

See the [defaults file](defaults/main.yml) for the available configuration options.
